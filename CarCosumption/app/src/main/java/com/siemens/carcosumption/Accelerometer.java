package com.siemens.carcosumption;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Vibrator;
import android.util.Log;
import java.util.Arrays;
import java.util.List;

public class Accelerometer implements SensorEventListener {

    private float lastX, lastY, lastZ;
    private SensorManager sensorManager;
    private Sensor accelerometer;

    private float deltaXMax = 0;
    private float deltaYMax = 0;
    private float deltaZMax = 0;

    private float deltaX = 0;
    private float deltaY = 0;
    private float deltaZ = 0;

    private float X = 0;
    private float Y = 0;
    private float Z = 0;

    private float alphaX = 0;
    private float alphaY = 0;
    private float alphaZ = 0;

    private float vibrateThreshold = 0;
    private float[] gravityV = {0.0f,0.0f,0.0f};
    final float alpha = 0.8f;

    public Vibrator v;

    Accelerometer(Context context/*SensorManager aSensorManager, Sensor aAccelerometer*/)
    {
        Log.i("Accelerometer", "Instance created!");

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
           Log.i("Accelerometer", "Success, accelerometer found!");
           accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
           sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
           vibrateThreshold = accelerometer.getMaximumRange() / 2;
       }
       else {
           Log.i("Accelerometer", "Failed to acquire accelerometer, not found!");
       }
       v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    public List<Float> getData()
    {
        // unfilterd data, last positon - data, data - gravity, max values from unfiltered data - TODO refactor name
        return Arrays.asList(X, Y, Z , deltaX, deltaY, deltaZ, alphaX, alphaY, alphaZ, deltaXMax, deltaYMax, deltaZMax);
    }

    protected void onResume() {
     //   super.onResume();
        sensorManager.registerListener(this, accelerometer,
                sensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
       sensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    // only using onSensorChanged
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        //only for deltaXYZ
        setMaxValues();

        //sensor values
        X = event.values[0];
        Y = event.values[1];
        Z = event.values[2];

        //gravity is calculated here
        gravityV[0] = alpha * gravityV[0] + (1 - alpha) * X;
        gravityV[1] = alpha * gravityV[1] + (1 - alpha)* Y;
        gravityV[2] = alpha * gravityV[2] + (1 - alpha) * Y;

        //acceleration retrieved from the event and the gravity is removed
        alphaX = X - gravityV[0];
        alphaY = Y - gravityV[1];
        alphaZ = Z - gravityV[2];

        deltaX = Math.abs(lastX - event.values[0]);
        deltaY = Math.abs(lastY - event.values[1]);
        deltaZ = Math.abs(lastZ - event.values[2]);

        //less than 2 is just noise, idk about that? Proly
        if(deltaX < 2)
            deltaX = 0;
        if(deltaY < 2)
            deltaY = 0;
        if(deltaZ < 2)
            deltaZ = 0;


        //saving values in last captured position
        lastX = event.values[0];
        lastY = event.values[1];
        lastZ = event.values[2];

        //this doesn't seem to work but I leave it here for the moment
        if ((deltaX > vibrateThreshold) || (deltaY > vibrateThreshold) || (deltaZ > vibrateThreshold)) {
            v.vibrate(50);
        }
    }

    public void setMaxValues(){
        if(deltaX > deltaXMax) {
            deltaXMax = deltaX;
        }
        if(deltaY > deltaYMax) {
            deltaYMax = deltaY;
        }
        if(deltaZ > deltaZMax) {
            deltaZMax = deltaZ;
        }
    }
}


