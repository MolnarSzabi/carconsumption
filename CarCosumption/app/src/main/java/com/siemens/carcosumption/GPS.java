package com.siemens.carcosumption;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

import mad.location.manager.lib.Interfaces.LocationServiceInterface;
import mad.location.manager.lib.Services.ServicesHelper;
import mad.location.*;
public class GPS implements LocationServiceInterface {

    private List<Location> m_lstGpsCoordinates = new ArrayList<>();

    GPS(){
        ServicesHelper.addLocationServiceInterface(this);
    }
    @Override
    public void locationChanged(Location location) {
        if (location == null) return;
//        if (loc.isFromMockProvider()) return;
        m_lstGpsCoordinates.add(location);
    }
}
