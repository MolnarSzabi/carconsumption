package com.siemens.carcosumption;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.os.Handler;

import com.siemens.exportcsv.fileprovider.CVSExporter;

import java.io.Console;
import java.math.BigDecimal;
import java.util.List;

import mad.location.manager.lib.Loggers.GeohashRTFilter;
import mad.location.manager.lib.Services.KalmanLocationService;

public class MainActivity extends Activity implements GPSCallback{

    private  boolean isRecording = false;
    private long startTime, timeBuff, updateTime = 0L ;
    private int seconds, minutes, milliSeconds , updateSeconds;
    private TextView stopWatch;
    private Handler handler = new Handler();
    private Handler viewHandler = new Handler();
    private Accelerometer accelerometer;
    private StringBuilder data = new StringBuilder();

    private TextView currentX, currentY, currentZ, maxX, maxY, maxZ;
    private TextView deltaX, deltaY, deltaZ, alphaX, alphaY, alphaZ;
    private TextView speedTextView;
    private List<Float> accelerometerData;
    public static Context appContext;

    private GPSManager gpsManager = null;
    private double speed = 0.0;
    Boolean isGPSEnabled=false;
    LocationManager locationManager;
    double currentSpeed,kmphSpeed;
    //TextView textview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            initializeViews();
            appContext = getApplicationContext();

            final Button recordBtn = findViewById(R.id.RecordButton);
            ViewCompat.setBackgroundTintList(recordBtn, ContextCompat.getColorStateList(appContext, android.R.color.holo_green_dark));

            final CVSExporter cvsExporter = new CVSExporter();
            data.append("timestamp,x,y,z,Abs(lastX - x),Abs(lastY - y),Abs(lastZ - )," +
                    " X - gravity, Y - Gravity, Z - Gravity, absMaxX, absMaxY, absMaxZ");

            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            final PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    "MyApp::MyWakelockTag");

            // todo watch out for these lines of code
            GPS gps = new GPS();
            KalmanLocationService A = new KalmanLocationService();
            GeohashRTFilter a = A.getGeoHashRTFilter();
            a.getDistanceAsIsHP();
            System.out.println("getDistanceAsIsHP" + a.getDistanceAsIsHP());

            final GPSManager gpsManager = new GPSManager(appContext, this);
            gpsManager.CheckGPSPermissions();
            gpsManager.IsGPSEnabled();

            recordBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(!isRecording)
                    {
                        wakeLock.acquire();

                        getCurrentSpeed(v);
                        ViewCompat.setBackgroundTintList(recordBtn, ContextCompat.getColorStateList(appContext, android.R.color.holo_red_dark));
                        isRecording = true;
                        startTime = SystemClock.uptimeMillis();
                        handler.postDelayed(runnable, 0);
                        accelerometer = new Accelerometer(appContext);
                        Log.i("MyApp", "Color changed to red, start recording!");
                    }
                    else
                    {
                        ViewCompat.setBackgroundTintList(recordBtn, ContextCompat.getColorStateList(appContext, android.R.color.holo_green_dark));
                        isRecording = false;
                        Log.i("MyApp", "Color changed to green, start recording!");
                        cvsExporter.export(data);
                        resetData();
                        wakeLock.release();
                    }
                }
            });
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void getCurrentSpeed(View view){
        //speedTextView.setText(getString(R.string.info));
        locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        gpsManager = new GPSManager(appContext, MainActivity.this);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if(isGPSEnabled) {
            gpsManager.startListening(getApplicationContext());
            gpsManager.setGPSCallback(this);
        } else {
            gpsManager.showSettingsAlert();
        }
    }
    @Override
    public void onGPSUpdate(Location location) {
        speed = location.getSpeed();
        currentSpeed = round(speed,3, BigDecimal.ROUND_HALF_UP);
        kmphSpeed = round((currentSpeed*3.6),3,BigDecimal.ROUND_HALF_UP);
        speedTextView.setText(kmphSpeed+"km/h");
    }
    @Override
    protected void onDestroy() {
        gpsManager.stopListening();
        gpsManager.setGPSCallback(null);
        gpsManager = null;
        super.onDestroy();
    }
    public static double round(double unrounded, int precision, int roundingMode) {
        BigDecimal bd = new BigDecimal(unrounded);
        BigDecimal rounded = bd.setScale(precision, roundingMode);
        return rounded.doubleValue();
    }

    static public Context getContext()
    {
        return appContext;
    }

    public void resetData(){
        startTime = 0L ;
        updateTime = 0L ;
        seconds = 0 ;
        updateSeconds = 0;
        minutes = 0 ;
        milliSeconds = 0 ;
        stopWatch.setText("00:00:00");
        handler.removeCallbacks(runnable);
    }

    public void initializeViews() {
        currentX = (TextView) findViewById(R.id.currentX);
        currentY = (TextView) findViewById(R.id.currentY);
        currentZ = (TextView) findViewById(R.id.currentZ);

        speedTextView = (TextView) findViewById(R.id.speed);

        deltaX = (TextView) findViewById(R.id.deltaX);
        deltaY = (TextView) findViewById(R.id.deltaY);
        deltaZ = (TextView) findViewById(R.id.deltaZ);

        alphaX = (TextView) findViewById(R.id.alphaX);
        alphaY = (TextView) findViewById(R.id.alphaY);
        alphaZ = (TextView) findViewById(R.id.alphaZ);

        maxX = (TextView) findViewById(R.id.maxX);
        maxY = (TextView) findViewById(R.id.maxY);
        maxZ = (TextView) findViewById(R.id.maxZ);
    }

    @SuppressLint("SetTextI18n")
    public void updateAccelerometerTextView()
    {
        currentX.setText(Float.toString(accelerometerData.get(0)));
        currentY.setText(Float.toString(accelerometerData.get(1)));
        currentZ.setText(Float.toString(accelerometerData.get(2)));
        deltaX.setText(Float.toString(accelerometerData.get(3)));
        deltaY.setText(Float.toString(accelerometerData.get(4)));
        deltaZ.setText(Float.toString(accelerometerData.get(5)));
        alphaX.setText(Float.toString(accelerometerData.get(6)));
        alphaY.setText(Float.toString(accelerometerData.get(7)));
        alphaZ.setText(Float.toString(accelerometerData.get(8)));
        maxX.setText(Float.toString(accelerometerData.get(9)));
        maxY.setText(Float.toString(accelerometerData.get(10)));
        maxZ.setText(Float.toString(accelerometerData.get(11)));
    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
        accelerometerData = accelerometer.getData();
        data.append("\n" + String.format("%02d:", minutes) + String.format("%02d:", seconds) + String.format("%02d:", milliSeconds) + "," +  accelerometerData.get(0) + "," + accelerometerData.get(1) + ","
                + accelerometerData.get(2) + "," + accelerometerData.get(3) + ","
                + accelerometerData.get(4) + "," + accelerometerData.get(5) + ","
                + accelerometerData.get(6) + "," + accelerometerData.get(7) + ","
                + accelerometerData.get(8) + ","+  accelerometerData.get(9) + ","
                + accelerometerData.get(10) + "," + accelerometerData.get(11));


        updateAccelerometerTextView();
        stopWatch = findViewById(R.id.StopWatch);
        updateTime = SystemClock.uptimeMillis() - startTime;
       // updateTime = timeBuff + millisecondTime;
        seconds = (int)(updateTime/1000);
        updateSeconds = seconds%60;
        minutes = seconds/60;
        milliSeconds = (int)(updateTime % 1000);
        stopWatch.setText(String.format("%02d", minutes) + ":" + String.format("%02d",updateSeconds) + ":" + String.format("%03d", milliSeconds));
        handler.postDelayed(this, 0);
        }
    };
}
