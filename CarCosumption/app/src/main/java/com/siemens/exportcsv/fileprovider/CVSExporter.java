package com.siemens.exportcsv.fileprovider;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.content.FileProvider;

import com.siemens.carcosumption.MainActivity;

import java.io.File;
import java.io.FileOutputStream;

public class CVSExporter {
    private Context context;

    public CVSExporter(/*Context aContext*/){
        this.context = MainActivity.getContext();
    }
        public void export(StringBuilder data){
            try{
                //saving the file into device
                FileOutputStream out = context.openFileOutput("data.csv",
                        Context.MODE_PRIVATE);
                out.write((data.toString()).getBytes());
                out.close();

                File fileLocation = new File(context.getFilesDir(), "data.csv");
                Uri path = FileProvider.getUriForFile(context,
                        "com.siemens.exportcsv.fileprovider", fileLocation);
                Intent fileIntent = new Intent(Intent.ACTION_SEND);
                fileIntent.setType("text/csv");
                fileIntent.putExtra(Intent.EXTRA_SUBJECT, "Data");
                fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                fileIntent.putExtra(Intent.EXTRA_STREAM, path);
                fileIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(fileIntent);

            }
            catch(Exception e){
                e.printStackTrace();
            }

        }
}
